// routes
package main

import (
	"github.com/julienschmidt/httprouter"
)

type Route struct {
	Name        string
	Method      string
	Path        string
	HandlerFunc httprouter.Handle
}

type Routes []Route

func AllRoutes() Routes {
	routes := Routes{
		Route{"Index", "GET", "/", index},
		Route{"GetUsers", "GET", "/users", getUsers},
		Route{"GetUserById", "GET", "/users/:userId", getUserById},
		Route{"DeleteUserById", "DELETE", "/users/:userId", delUser},
		Route{"AddUser", "POST", "/users", addUser},
		Route{"AddUser", "PUT", "/users/:userId", updateUser},
	}
	return routes
}
