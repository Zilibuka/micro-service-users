// handlers
package main

import (
	"fmt"
	"net/http"

	"database/sql"
	"encoding/json"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id       string `json:"id"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type Users struct {
	Users []*User
}

var salt string = "ebanyrot"
var db, sqlErr = sql.Open("mysql", "root:4532010@/ms_user-accounts")

// Handler for route GET "/"
func index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	log.WithFields(log.Fields{"action": "Request", "method": r.Method, "path": r.URL.String()}).Info()
	fmt.Fprintf(w, "ms user accounts")
}

// Handler for route GET "/users/[id]"
func getUserById(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	log.WithFields(log.Fields{"action": "Request", "method": r.Method, "path": r.URL.String()}).Info()

	rows := db.QueryRow("SELECT * FROM users WHERE id='" + ps.ByName("userId") + "'")

	var prs_id string
	var prs_email string
	var prs_password string
	users := &Users{}

	if scanErr := rows.Scan(&prs_id, &prs_password, &prs_email); scanErr != nil {
		log.WithFields(log.Fields{"action": "MySql result scan", "Error": scanErr}).Error()
		w.WriteHeader(http.StatusNoContent)
	} else {
		user := &User{prs_id, prs_password, prs_email}
		users.Users = append(users.Users, user)
		jsonUsers, _ := json.Marshal(users.Users)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, string(jsonUsers))
	}
}

// Handler for route GET "/users"
func getUsers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	log.WithFields(log.Fields{"action": "Request", "method": r.Method, "path": r.URL.String()}).Info()

	if rows, queryErr := db.Query("SELECT * FROM users"); queryErr != nil {
		log.WithFields(log.Fields{"action": "MySql Query", "Error": queryErr}).Error()
	} else {
		var prs_id string
		var prs_email string
		var prs_password string
		users := &Users{}

		for rows.Next() {
			if scanErr := rows.Scan(&prs_id, &prs_password, &prs_email); scanErr != nil {
				log.WithFields(log.Fields{"action": "MySql result scan", "Error": scanErr}).Error()
			} else {
				user := &User{prs_id, prs_password, prs_email}
				users.Users = append(users.Users, user)
			}
		}
		jsonUsers, _ := json.Marshal(users.Users)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, string(jsonUsers))
	}
}

// Handler for route DELETE "/users/[id]"
func delUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	log.WithFields(log.Fields{"action": "Request", "method": r.Method, "path": r.URL.String()}).Info()

	if _, queryErr := db.Query("DELETE FROM users WHERE id='" + ps.ByName("userId") + "'"); queryErr != nil {
		log.WithFields(log.Fields{"action": "MySql Query", "Error": queryErr}).Error()
		w.WriteHeader(http.StatusBadRequest)
	}
}

// Handler for route POST "/users"
func addUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	log.WithFields(log.Fields{"action": "Request", "method": r.Method, "path": r.URL.String()}).Info()
	decoder := json.NewDecoder(r.Body)
	var tmpUser User
	if decErr := decoder.Decode(&tmpUser); decErr != nil {
		log.WithFields(log.Fields{"action": "JSON decode", "Error": decErr}).Error()
		w.WriteHeader(http.StatusBadRequest)
	} else {
		if encryptedPassBytes, bCryptErr := bcrypt.GenerateFromPassword([]byte(tmpUser.Password+salt), 14); bCryptErr != nil {
			log.WithFields(log.Fields{"action": "bcrypt Pass Gen", "Error": bCryptErr}).Error()
			w.WriteHeader(http.StatusBadRequest)
		} else {
			if rows, queryErr := db.Exec(" INSERT INTO users (password, email) VALUES ('" + string(encryptedPassBytes) + "','" + tmpUser.Email + "')"); queryErr != nil {
				log.WithFields(log.Fields{"action": "MySql Query", "Error": queryErr}).Error()
				w.WriteHeader(http.StatusBadRequest)
			} else {
				newId, _ := rows.LastInsertId()
				rows := db.QueryRow("SELECT * FROM users WHERE id='" + strconv.FormatInt(newId, 10) + "'")

				var prs_id string
				var prs_email string
				var prs_password string
				users := &Users{}

				if scanErr := rows.Scan(&prs_id, &prs_password, &prs_email); scanErr != nil {
					log.WithFields(log.Fields{"action": "MySql result scan", "Error": scanErr}).Error()
					w.WriteHeader(http.StatusBadRequest)
				} else {
					user := &User{prs_id, prs_password, prs_email}
					users.Users = append(users.Users, user)
					jsonUsers, _ := json.Marshal(users.Users)
					w.WriteHeader(http.StatusCreated)
					w.Header().Set("Content-Type", "application/json")
					fmt.Fprintf(w, string(jsonUsers))
				}
			}
		}

	}
	defer r.Body.Close()
}

// Handler for route PUT "/users/[id]"
func updateUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	log.WithFields(log.Fields{"action": "Request", "method": r.Method, "path": r.URL.String()}).Info()
	decoder := json.NewDecoder(r.Body)
	var tmpUser User
	if decErr := decoder.Decode(&tmpUser); decErr != nil {
		log.WithFields(log.Fields{"action": "JSON decode", "Error": decErr}).Error()
		w.WriteHeader(http.StatusBadRequest)
	} else {
		if _, queryErr := db.Exec("UPDATE users SET password = '" + tmpUser.Password + "', email = '" + tmpUser.Email + "' WHERE id = '" + ps.ByName("userId") + "'"); queryErr != nil {
			log.WithFields(log.Fields{"action": "MySql Query", "Error": queryErr}).Error()
			w.WriteHeader(http.StatusBadRequest)
		} else {
			rows := db.QueryRow("SELECT * FROM users WHERE id='" + ps.ByName("userId") + "'")

			var prs_id string
			var prs_email string
			var prs_password string
			users := &Users{}

			if scanErr := rows.Scan(&prs_id, &prs_password, &prs_email); scanErr != nil {
				log.WithFields(log.Fields{"action": "MySql result scan", "Error": scanErr}).Error()
				w.WriteHeader(http.StatusBadRequest)
			} else {
				user := &User{prs_id, prs_password, prs_email}
				users.Users = append(users.Users, user)
				jsonUsers, _ := json.Marshal(users.Users)
				w.Header().Set("Content-Type", "application/json")
				fmt.Fprintf(w, string(jsonUsers))
			}

		}
	}
	defer r.Body.Close()
}
