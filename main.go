// main project main.go
package main

import (
	"net/http"

	log "github.com/sirupsen/logrus"
)

func main() {
	router := NewRouter(AllRoutes())
	log.Fatal(http.ListenAndServe(":8000", router))
}
